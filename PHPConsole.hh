<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\debug\PHPConsole
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use \Exception;
	use PhpConsole\Connector;
	use PhpConsole\Helper;
	use PhpConsole\Handler;
	
	<<singleton>>
	class PHPConsole extends Plugin
	{
		private Handler $handler;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):PHPConsole
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public function __construct()
		{
			parent::__construct();
			
			$this->handler=Handler::getInstance();
			
			$this->handler->start();
		}
		
		public function getHandler():Handler
		{
			return $this->handler;
		}
		
		public function debug(mixed $var):this
		{
			$this->handler->debug($var);
			return $this;
		}
		
		public function handleException(Exception $exception):void
		{
			$this->handler->handleException($exception);
		}
	}
}
